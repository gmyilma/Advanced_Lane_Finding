
Advanced Lane finding



The goals / steps of this project are the following:

Compute the camera calibration matrix and distortion coefficients given a set of chessboard images. Apply a distortion correction to raw images. Use color transforms, gradients, etc., to create a thresholded binary image. Apply a perspective transform to rectify binary image ("birds-eye view"). Detect lane pixels and fit to find the lane boundary. Determine the curvature of the lane and vehicle position with respect to center. Warp the detected lane boundaries back onto the original image. Output visual display of the lane boundaries and numerical estimation of lane curvature and vehicle position.

The following series of acitons has been performed:

Camera Calibration
Compute the camera calibration matrix and distortion coefficients given a set of chessboard images.
Used a distortion correction to raw images.
Used a distortion correction to test images.
Used color transforms, gradients, etc., to create a thresholded binary image.
Perspective transform
Lane Curvature

For details of the implementation please check:

https://github.com/gmyilma/Advanced_Lane_Finding/blob/master/.ipynb_checkpoints/AdvancedLaneFind-checkpoint.ipynb

